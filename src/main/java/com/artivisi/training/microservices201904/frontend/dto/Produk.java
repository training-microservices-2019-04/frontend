package com.artivisi.training.microservices201904.frontend.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Produk {
    private String id;
    private String kode;
    private String nama;
    private BigDecimal harga;
}
